package simulations

import common._

class Wire {
  private var sigVal = false
  private var actions: List[Simulator#Action] = List()

  def getSignal: Boolean = sigVal
  
  def setSignal(s: Boolean) {
    if (s != sigVal) {
      sigVal = s
      actions.foreach(action => action())
    }
  }

  def addAction(a: Simulator#Action) {
    actions = a :: actions
    a()
  }
}

abstract class CircuitSimulator extends Simulator {

  val InverterDelay: Int
  val AndGateDelay: Int
  val OrGateDelay: Int

  def probe(name: String, wire: Wire) {
    wire addAction {
      () => afterDelay(0) {
        println(
          "  " + currentTime + ": " + name + " -> " +  wire.getSignal)
      }
    }
  }

  def inverter(input: Wire, output: Wire) {
    def invertAction() {
      val inputSig = input.getSignal
      afterDelay(InverterDelay) { output.setSignal(!inputSig) }
    }
    input addAction invertAction
  }

  def andGate(a1: Wire, a2: Wire, output: Wire) {
    def andAction() {
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(AndGateDelay) { output.setSignal(a1Sig & a2Sig) }
    }
    a1 addAction andAction
    a2 addAction andAction
  }

  def orGate(a1: Wire, a2: Wire, output: Wire) {
    def orAction() {
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(OrGateDelay) { output.setSignal(a1Sig | a2Sig) }
    }
    a1 addAction orAction
    a2 addAction orAction
  }
  
  def orGate2(a1: Wire, a2: Wire, output: Wire) {
    val i1, i2, and = new Wire
    inverter(a1, i1)
    inverter(a2, i2)
    andGate(i1, i2, and)
    inverter(and, output)
  }

  def demux(in: Wire, c: List[Wire], out: List[Wire]) {
    def _demux(prefix: String, in: Wire, c: List[Wire], out: List[Wire]) {
      c.headOption match {
        case Some(control) =>
          val outIn0, outIn1, invControl = new Wire
          probe(s"control $prefix", control)
          inverter(control, invControl)
          andGate(in, invControl, outIn0)
          andGate(in, control,    outIn1)
          probe(s"out ${prefix}0", outIn0)
          probe(s"out ${prefix}1", outIn1)
          val outs = out.grouped(out.length/2)
          _demux(s"${prefix}1", outIn1, c.tail, outs.next)
          _demux(s"${prefix}0", outIn0, c.tail, outs.next)

        case _ =>
          val o = out.head
          def wireDirectly() = o.setSignal(in.getSignal)
          in addAction wireDirectly
          probe(s"demux out $prefix", o)
      }
    }
    _demux("", in, c, out)
  }
}

object Circuit extends CircuitSimulator {
  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5

  def andGateExample {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    probe("in1", in1)
    probe("in2", in2)
    probe("out", out)
    in1.setSignal(false)
    in2.setSignal(false)
    run

    in1.setSignal(true)
    run

    in2.setSignal(true)
    run
  }

  def demuxExample {
    val in, c0, c1, o0, o1, o2, o3 = new Wire
    val controls = List(c1, c0)
    val outs = List(o3, o2, o1, o0)
    demux(in, controls, outs)

    in.setSignal(false)
    controls.foreach(_.setSignal(false))
    run

    in.setSignal(true)
    run
  }
  //
  // to complete with orGateExample and demuxExample...
  //
}

object CircuitMain extends App {
  // You can write tests either here, or better in the test class CircuitSuite.
  Circuit.andGateExample
  Circuit.demuxExample
}
