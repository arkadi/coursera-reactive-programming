package simulations

import math.random

class EpidemySimulator extends Simulator {

  def randomBelow(i: Int) = (random * i).toInt

  protected[simulations] object SimConfig {
    val population: Int = 300
    val roomRows: Int = 8
    val roomColumns: Int = 8
    val moveWithinDays = 5
    val prevalenceRate = 1
    val trRate = 40
    val sickAfter = 6
    val maybeDiesAfter = 14
    val immuneAfter = 16
    val healthyAfter = 18
    val deathRate = 25
  }

  import SimConfig._

  val persons: List[Person] = (0 until population).map { i => 
    val p = new Person(i)
    if (i < population*prevalenceRate/100) p.infect // head is corrupted by test but checked by "prevalence" test, wtf!
    p
  } .toList

  class Person (val id: Int) {
    var infected = false
    var sick = false
    var immune = false
    var dead = false

    var row = randomBelow(roomRows)
    var col = randomBelow(roomColumns)

    setupMove()

    def neighbourhood(row: Int, col: Int) = {
      def wrap(limit: Int)(pos: Int) = if (pos >= limit) pos%limit else if (pos < 0) pos+limit else pos
      def wrapR = wrap(roomRows) _
      def wrapC = wrap(roomColumns) _
      Set( (row, wrapC(col+1)), (wrapR(row+1), col), (row, wrapC(col-1)), (wrapR(row-1), col) )
    }

    def setupMove() {
      afterDelay(randomBelow(moveWithinDays)+1)(lookAround)
    }

    def lookAround() {
      if (dead) return
      val rooms = neighbourhood(row, col)
      val neighbours = persons.filter(p => rooms.contains((p.row, p.col))).groupBy(p => (p.row, p.col))
      val badRooms = neighbours.filter { case (room, people) => people.exists(p => p.sick || p.dead) } .keys
      val goodRooms = rooms -- badRooms
      if (goodRooms.nonEmpty)
        (move _).tupled(goodRooms.drop(randomBelow(goodRooms.size)).head)
      setupMove()
    }

    def move(row: Int, col: Int) {
      this.row = row
      this.col = col
      party()
    }

    def party() {
      if (!infected) {
        val roomies = persons.filter(p => p.row == row && p.col == col)
        if (roomies.exists(_.infected) && randomBelow(100) < trRate)
          infect()
      }
    }

    def infect() {
      infected = true
      afterDelay(sickAfter) { if (!dead) { // corrupted by "dead person stays dead" test and also by on-site tests
        sick = true
        afterDelay(maybeDiesAfter - sickAfter) {
          if (randomBelow(100) < deathRate)
            dead = true
          else
            afterDelay(immuneAfter - maybeDiesAfter) {
              sick = false
              immune = true
              afterDelay(healthyAfter - immuneAfter) {
                infected = false
                immune = false
              }
            }
        }
      }}
    }
  }
}
