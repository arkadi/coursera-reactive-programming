package simulations

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CircuitSuite extends CircuitSimulator with FunSuite {
  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5
  
  test("andGate example") {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "and 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === false, "and 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "and 3")
  }

  test("orGate example") {
    val in1, in2, out = new Wire
    orGate(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    assert(out.getSignal === false, "or 1")

    in1.setSignal(true)
    run
    assert(out.getSignal === true, "or 2")

    in2.setSignal(true)
    run
    assert(out.getSignal === true, "or 3")

    in1.setSignal(false)
    run
    assert(out.getSignal === true, "or 4")
  }

  test("orGate2 example") {
    val in1, in2, out = new Wire
    orGate2(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    assert(out.getSignal === false, "or2 1")

    in1.setSignal(true)
    run    
    assert(out.getSignal === true, "or2 2")
    
    in2.setSignal(true)
    run
    assert(out.getSignal === true, "or2 3")
    
    in1.setSignal(false)
    run
    assert(out.getSignal === true, "or2 4")
  }

  test("demux") {
    val in, c0, c1, o0, o1, o2, o3 = new Wire
    val controls = List(c1, c0)
    val outs = List(o3, o2, o1, o0)
    demux(in, controls, outs)

    in.setSignal(false)
    controls.foreach(_.setSignal(false))
    run
    assert(outs.filterNot(_.getSignal == false).isEmpty)

    in.setSignal(true)
    run
    assert(outs.reverse.head.getSignal ===  true)
    assert(outs.reverse.tail.forall(_.getSignal == false))

    c0.setSignal(true)
    c1.setSignal(true)
    run
    assert(outs.head.getSignal ===  true)
    assert(outs.tail.forall(_.getSignal == false))
  }
}
