package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("isEmpty, findMin works and deleteMin removes elements in correct order") = forAll { (l: List[A]) =>
    val h = l.foldLeft(empty)( (h, elem) => insert(elem, h) )
    @annotation.tailrec
    def collect(acc: List[A], h: H): List[A] = if (isEmpty(h)) acc.reverse else collect(findMin(h) :: acc, deleteMin(h))
    collect(List(), h) == l.sorted
  }

  property("meld works") = forAll { (h1: H, h2: H) =>
    !isEmpty(h1) && !isEmpty(h2) ==> {
      val m1 = findMin(h1)
      val m2 = findMin(h2)
      findMin(meld(h1, h2)) == (if (m1 < m2) m1 else m2)
    }
  }

  lazy val genHeap: Gen[H] = for {
    a <- arbitrary[A]
    h <- frequency( (5, genHeap), (1, value(empty)) )
  } yield insert(a, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)
}
