Your overall score for this assignment is 10.00 out of 10.00

Your solution passed all of our tests, congratulations! You obtained the maximal test
score of 10.00.

Our automated style checker tool could not find any issues with your code. You obtained the maximal
style score of 0.00.
