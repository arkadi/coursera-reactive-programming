package kvstore

import akka.actor.OneForOneStrategy
import akka.actor.PoisonPill
import akka.actor.SupervisorStrategy
import akka.actor.SupervisorStrategy.Restart
import akka.actor.Terminated
import akka.actor.{ OneForOneStrategy, Props, ActorRef, Actor, Cancellable }
import akka.pattern.{ ask, pipe }
import akka.util.Timeout
import kvstore.Arbiter._
import scala.collection.mutable
import scala.concurrent.duration._

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

import Replica._
import Replicator._
import Persistence._

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor {

  // should I use pre/post(Re)Start hooks?
  arbiter ! Join

  val kv = mutable.Map.empty[String, String]
  // a map from secondary replicas to replicators
  val secondaries = mutable.Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]

  def receive = {
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica)
  }

  // translate client-specific id into Replica-unique id
  var _id = 1l
  val requesters = mutable.Map.empty[Long, (ActorRef, Long)]
  def remember(requester: ActorRef, id: Long) = {
    val r = _id; _id += 1
    requesters += r -> (requester, id)
    r
  }
  def translate(id: Long) = requesters.get(id) match {
    case None => println(s"unable to translate $id"); -1l
    case Some((_, _id)) => _id
  }
  def reply(id: Long, msg: Any) {
    requesters.get(id) match {
      case None => println(s"too late, no recipient for $id $msg")
      case Some((actor, _)) => actor ! msg; requesters -= id
    }
  }

  // reliable persistor wrapper and persistence tracker
  val persistor = context.actorOf(Props(classOf[ReliablePersistence], persistenceProps))
  val persisted = mutable.Set.empty[Long]

  // replication tracking
  // TODO move to separate actor
  import context.dispatcher
  case class Tick(id: Long)
  val failAfter = 1.second
  val pendingSchedules = mutable.Map.empty[Long, Cancellable]
  val pendingReplicators = mutable.Map.empty[Long, Set[ActorRef]].withDefaultValue(Set.empty)
  val pendingIds = mutable.Map.empty[ActorRef, Set[Long]].withDefaultValue(Set.empty)
  def replicate(op: Replicate) {
    replicators.foreach { replicator =>
      replicator ! op
      pendingIds.update(replicator, pendingIds(replicator) + op.id)
    }
    pendingReplicators.update(op.id, replicators) // 'replicators' is immutable set
    pendingSchedules.update(op.id, context.system.scheduler.scheduleOnce(failAfter, self, Tick(op.id)))
  }

  def forgetReplicators(id: Long) {
    pendingReplicators(id).foreach { replicator =>
      pendingIds.update(replicator, pendingIds(replicator) - id)
    }
    pendingReplicators -= id
  }

  def maybeAck(id: Long) {
    if (persisted.contains(id) && pendingReplicators(id).isEmpty && pendingSchedules.contains(id)) {
      persisted -= id
      pendingReplicators -= id
      pendingSchedules(id).cancel()
      pendingSchedules -= id
      // id is cleaned from pendingIds(replicator) in reader():Replicated
      reply(id, OperationAck(translate(id)))
    }
  }

  def waive(replicator: ActorRef) {
    pendingIds(replicator).foreach { id =>
      pendingReplicators.update(id, pendingReplicators(id) - replicator)
      maybeAck(id)
    }
    pendingIds -= replicator
  }

  /* Behavior for the leader role. */
  val leader: Receive = {

    case Get(key, id) =>
      sender ! GetResult(key, kv.get(key), id)

    case Replicas(newReplicas) =>
      val replicas = secondaries.keySet
      // kill replicators not in replica set
      (replicas -- newReplicas).foreach { replica =>
        val replicator = secondaries(replica)
        secondaries -= replica
        replicator ! PoisonPill
        waive(replicator)
      }
      // create replicators for new replicas
      (newReplicas -- replicas - self).foreach { replica =>
        val replicator = context.actorOf(Replicator.props(replica))
        context.watch(replicator)
        val id = 0l
        kv.foreach { case (k, v) =>
          replicator ! Replicate(k, Some(v), id)
        }
        secondaries.update(replica, replicator)
      }
      replicators = secondaries.values.toSet

    case Terminated(child) =>
      secondaries.find { case (_, replicator) => child == replicator } .foreach { case (replica, replicator) =>
        secondaries -= replica
        waive(replicator)
      }
      replicators = secondaries.values.toSet

    case Insert(key, value, id) =>
      kv += (key -> value)
      val _id = remember(sender, id)
      replicate(Replicate(key, Some(value), _id))
      persistor ! Persist(key, Some(value), _id)

    case Remove(key, id) =>
      kv -= key
      val _id = remember(sender, id)
      replicate(Replicate(key, None, _id))
      persistor ! Persist(key, None, _id)

    // id == 0 is a special case of fresh replicator getting all values from 'kv' at once
    case Replicated(key, id) => if (id > 0) {
      if (pendingReplicators(id).nonEmpty) {
        pendingReplicators.update(id, pendingReplicators(id) - sender)
        pendingIds.update(sender, pendingIds(sender) - id)
      }
      maybeAck(id)
    }

    case Persisted(key, id) =>
      persisted += id
      maybeAck(id)

    case PersistFailed(key, id) =>
      forgetReplicators(id)
      pendingSchedules.get(id).foreach { c => c.cancel(); pendingSchedules -= id }
      reply(id, OperationFailed(translate(id)))

    case Tick(id) =>
      forgetReplicators(id)
      pendingSchedules -= id
      reply(id, OperationFailed(translate(id)))
  }

  var expectedSeq = 0l
  /* Behavior for the replica role. */
  val replica: Receive = {

    case Get(key, id) =>
      sender ! GetResult(key, kv.get(key), id)

    case Snapshot(key, valueOpt, seq) =>
      if (seq == expectedSeq) {
        if (valueOpt.nonEmpty) kv += (key -> valueOpt.get)
        else                   kv -= key
        val id = remember(sender, seq)
        persistor ! Persist(key, valueOpt, id)
        expectedSeq += 1
      } else if (seq < expectedSeq)
        sender ! SnapshotAck(key, seq) // XXX we may sent Ack for still not-persisted KV
      else println(s"replica got seq $seq while expecting $expectedSeq")

    case Persisted(key, id) =>
      reply(id, SnapshotAck(key, translate(id)))
  }
}

class ReliablePersistence(persistenceProps: Props) extends Actor {
  import context.dispatcher

  // the unreliable persistor
  val persistor = context.actorOf(persistenceProps)

  //import context.system
  case object Tick
  val failAfter = 1.second
  val retry = 100.milliseconds
  val schedule = context.system.scheduler.schedule(retry, retry, self, Tick)
  override def postStop() { schedule.cancel() }

  case class Sent(when: Long, persist: Persist, requester: ActorRef)
  val inflightOps = collection.mutable.Map.empty[Long, Sent]
  val latestValue = collection.mutable.Map.empty[String, Option[String]]

  private def cleanup(op: Persist) {
    inflightOps -= op.id
    if (latestValue(op.key) == op.valueOption) latestValue -= op.key
  }

  def receive: Receive = {
    // proxy to unreliable persistor and remember the operation and latest value for key
    case op @ Persist(key, v, id) =>
      persistor ! op
      inflightOps += (id -> Sent(System.nanoTime, op, sender))
      latestValue.update(key, v)

    case reply @ Persisted(key, id) =>
      inflightOps.get(id) match {
        case None => println(s"too late, already replied to $id for key $key")
        case Some(Sent(_, op, actor)) =>
          actor ! reply
          cleanup(op)
      }

    case Tick =>
      val now = System.nanoTime
      val ancient = now - failAfter.toNanos
      val retryable = now - retry.toNanos
      // non-optimal loop
      inflightOps.foreach { case (_, sent) =>
        val op = sent.persist
        if (sent.when < ancient) {
          sent.requester ! PersistFailed(op.key, op.id)
          cleanup(op)

        } else if (sent.when < retryable) {
          if (latestValue(op.key) == op.valueOption)
            persistor ! op
          else {
            sent.requester ! Persisted(op.key, op.id) // hope later the key will be overwritten anyway
            cleanup(op)
          }
        }
      }
  }
}
