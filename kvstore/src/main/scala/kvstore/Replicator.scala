package kvstore

import akka.actor.{Actor, ActorRef, Props, ReceiveTimeout}
import scala.collection.mutable
import scala.concurrent.duration._

object Replicator {
  case class Replicate(key: String, valueOption: Option[String], id: Long)
  case class Replicated(key: String, id: Long)

  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor {
  import Replicator._
  import Replica._
  import context.dispatcher

  // map from sequence number to pair of sender and request
  val requests = mutable.Map.empty[Long, (ActorRef, Replicate, Long)]
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  //var pending = Vector.empty[Snapshot]
  var _seq = 0l
  def nextSeq = { val r = _seq; _seq += 1; r }

  case object Tick
  val retry = 50.milliseconds
  val schedule = context.system.scheduler.schedule(retry, retry, self, Tick)
  override def postStop() { schedule.cancel() }

  /* Behavior for the Replicator. */
  def receive: Receive = {

    case op @ Replicate(key, valueOpt, id) =>
      val seq = nextSeq
      replica ! Snapshot(key, valueOpt, seq)
      requests.update(seq, (sender, op, System.nanoTime))

    case op @ SnapshotAck(key, seq) =>
      requests.get(seq) match {
        case None => println(s"Unexpected $op received")
        case Some((_parent, rop, _)) =>
          _parent ! Replicated(key, rop.id)
          requests -= seq
      }

    case Tick =>
      val now = System.nanoTime
      requests.toList.sortBy(_._1).foreach { case (seq, (_, op @ Replicate(key, valueOpt, id), when)) =>
        if (when + 2*retry.toNanos < now) replica ! Snapshot(key, valueOpt, seq)
      }

  }

}
